var app = angular.module('chatRoomApp');

app.factory('messageFactory', function () {
    var messages = [];
    
    return {
        setMessages: function(arg) {
            messages = arg;
        },
        getMessages: function() {
            return messages;
        },
        pushMessage: function(arg) {
            var duplicate = false;
            for(var i=0;i<messages.length;i++) {
                console.log(arg.group);
                console.log(messages[i].group);
                if(messages[i].group==arg.group) {
                    duplicate = true;
                    break;
                }
            }
            
            if(!duplicate) {
                messages.push(arg);
            }
        },
        pushSingleMessage: function(data) {
            for(var i=0;i<messages.length;i++) {
                if(messages[i].group===data.group && messages[i].msgs[messages[i].msgs.length-1] && messages[i].msgs[messages[i].msgs.length-1].date != data.date) {
                    messages[i].msgs.push(data);
                    break;
                }
            }
        }
    }
});