var app = angular.module('chatRoomApp');

app.factory('socket', function (socketFactory) {
  return socketFactory();
});