var app = angular.module('chatRoomApp');

app.factory('headerFactory', [function() {
    var title = '';
    var groupName = '';
    
    return {
        setTitle: function(arg) {
            title = arg;
        },
        getTitle: function() {
            return title;
        },
        setGroupName: function(arg) {
            groupName = arg;
        },
        getGroupName: function() {
            return groupName;
        }
    }
}])