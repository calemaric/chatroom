var app = angular.module('chatRoomApp');

app.directive('mySnippet', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/snippet.html',
        controller: 'MainCtrl',
        scope: {
            'snippet': '='
        },
        link: function (scope, elem, attrs) {
            scope.group = scope.snippet.group;
            scope.name = scope.snippet.name;
            scope.msg = scope.snippet.msg;
            scope.date = scope.snippet.date;
            scope.email = scope.snippet.email;
            scope.moveToTop = false;
            scope.index = attrs.index;
            scope.newMessage = 0;

            scope.$watch(function () {
                return scope.snippet.date;
            }, function (n, o) {
                if (n > o) {
                    scope.moveToTop = true;
                    scope.name = scope.snippet.name;
                    scope.msg = scope.snippet.msg;
                    scope.date = scope.snippet.date;
                    scope.email = scope.snippet.email;
                }
            })

            scope.$watch(function () {
                return attrs.focus;
            }, function (n, o) {
                if (n == scope.index) {
                    elem.children(':first').addClass('groupActive');
                } else {
                    elem.children(':first').removeClass('groupActive');
                }
            })
        }
    }
})