var app = angular.module('chatRoomApp');

app.controller('HeaderCtrl', ['$scope', 'headerFactory','$window', function($scope,headerFactory,$window) {
    $scope.title = 'Chat Room';
    
    $scope.$watch(function() {
        return headerFactory.getTitle();
    }, function(e) {
        $scope.title = e;
    })
    
    angular.element($window).bind('focus', function() {
        if(headerFactory.getGroupName()) {
            $scope.title = headerFactory.getGroupName()+" - ";
        }
        $scope.$apply();
    })
}]);