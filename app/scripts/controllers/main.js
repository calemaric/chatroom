'use strict';

/**
 * @ngdoc function
 * @name chatRoomApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the chatRoomApp
 */
angular.module('chatRoomApp')
  .controller('MainCtrl',['$scope', 'socket', '$location', 'headerFactory', 'messageFactory', '$cookies', '$state', function ($scope,socket,$location,headerFactory,mFactory,$cookies,$state) {
    $scope.name = $cookies.get('username');
    $scope.email = $cookies.get('email');
    $scope.showGroup = false;
    $scope.group = '';
    $scope.messages = mFactory.getMessages();
    $scope.snippets = [];
    $scope.newMessage = 0;
    $scope.focus;
    
    if(mFactory.getMessages().length==0) {
        socket.emit('getGroupMessages');
    }
    
     socket.on('groupMessages', function(message) {
    	if(message) {
    	    $scope.messages.push(message);
    	    mFactory.pushMessage(message);
    	    
    	    if(message.msgs.length>0) {
  	        $scope.snippets.push({
  	            group: message.group,
  	            name: message.msgs[message.msgs.length-1].name,
  	            date: message.msgs[message.msgs.length-1].date,
  	            msg: message.msgs[message.msgs.length-1].msg,
  	            email: message.msgs[message.msgs.length-1].email
  	        })
    	    } else {
    	      $scope.snippets.push( {
    	        group:message.group,
    	        name: '',
    	        date: '',
    	        msg: '',
    	        email: ''
    	      })
    	    }
    	}
    });
    
    socket.on('message', function(msg) {
        if(msg.email == $cookies.get('email')) {
            msg.sent=true;
        } else {
        	if(msg.group == $scope.group) {
        	    msg.sent=false;
        	}
        }
        
        mFactory.pushSingleMessage(msg);

        $scope.snippets.forEach(function(sn) {
            if(sn.group==msg.group) {
                sn.msg = msg.msg;
                sn.name = msg.name;
                sn.date = msg.date;
                sn.email = msg.email;
                sn.messageNum = $scope.newMessage;
            }
        })
    })
    
    $scope.sendGroup = function() {
      socket.emit('createGroup',$scope.group);
      $scope.snippets.push( {
    	        group:$scope.group,
    	        name: '',
    	        date: '',
    	        msg: '',
    	        email: ''
    	});
    	$state.go('groups.groupPage',{groupName: $scope.group.toLowerCase()});
    	$('#myModal').modal('toggle');
    
      headerFactory.setTitle($scope.group+" - ");
      headerFactory.setGroupName($scope.group);
    }
    
    $scope.joinGroup = function (data,index) {
      socket.emit('joinGroup',data);
      headerFactory.setTitle(data+" - ");
      headerFactory.setGroupName(data);
      $scope.showGroup = true;
      $scope.focus = index;
    }
    
    $scope.go = function(arg) {
      var arg = arg.toLowerCase();
      $location.path('/groups/'+arg);
    }
}]);
