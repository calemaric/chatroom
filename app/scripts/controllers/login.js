var app = angular.module('chatRoomApp');


app.controller('LoginCtrl', ['$scope','socket','$location', '$cookies', function ($scope,socket,$location,$cookies) {
    $scope.username;
    $scope.email;
    $scope.loginForm;
    
    socket.on('connect', function () {
        console.log('dobrodosli');
    });
    
    socket.on("disconnect",function() {
        console.log('disconnected');
    })
    
   
    $scope.setName = function() {
        if($scope.loginForm.$valid) {
            socket.emit('identify',{
                name:$scope.username,
                email: $scope.email
                });
            $cookies.put('username',$scope.username);
            $cookies.put('email',$scope.email);
            $scope.go();
        } 
    }
    
    $scope.go = function() {
        $location.path('/groups');
    }
}]);
