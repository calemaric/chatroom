var app = angular.module('chatRoomApp');


app.controller('GroupCtrl', ['$scope','socket', '$stateParams', '$cookies', 'headerFactory','$window','messageFactory','md5', function ($scope,socket,$stateParams,$cookies,headerFactory,$window,mFactory,md5) {
    $scope.messages = [];
    $scope.message = '';
    $scope.allMessages = mFactory.getMessages();
    $scope.typingName;
    $scope.tabVisible = true;
    $scope.unreadMessages = 0;
    $scope.group = $stateParams.groupName;
    $scope.index;
    $scope.showName;

    var title = headerFactory.getTitle();

    if($scope.allMessages.length>0) {
        console.log($scope.allMessages);
        for(var i =0;i<$scope.allMessages.length;i++) {
            if($scope.allMessages[i].group == $scope.group ) {
                $scope.index = i;
                $scope.allMessages[i].msgs.forEach(function(msg) {
                    if(msg.name == $cookies.get("username")) {
                        msg.sent = true;
                        $scope.messages.push(msg);
                    } else {
                        msg.sent = false;
                        $scope.messages.push(msg);
                    }
                })
            }
        }
        
    } else {
        headerFactory.setGroupName($stateParams.groupName);
        socket.emit('identify',{
            name:$cookies.get('username'),
            email:$cookies.get('email')
            });
        socket.emit('joinGroup', $stateParams.groupName);
        socket.emit('getMessages');
    }
    
    angular.element($window).bind('focus', function() {
        $scope.tabVisible = true;
        $scope.unreadMessages = 0;
        $scope.scroll();
    }).bind('blur', function() {
        $scope.tabVisible = false;
    });
    
    $scope.scroll = function() {
        $("#msgContainer").animate({ scrollTop: $("#msgContainer").prop('scrollHeight') },10);
    }
    
    socket.on('message', function(message) {
        if(message.email == $cookies.get('email')) {
            message.sent=true;
            $scope.messages.push(message);
        } else {
        	if(message.group == $scope.group) {
        	    message.sent=false;
        	    $scope.messages.push(message);
        	}
        	
        	if(!$scope.tabVisible) {
                $scope.unreadMessages++;
        	    headerFactory.setTitle("("+$scope.unreadMessages+") "+title);
        	}
        }
        
        $scope.scroll();
    })
    
    socket.on('typing', function(name) {
        if(name && name!=$cookies.get('username')) {
            $scope.typingName = name;
            $scope.scroll();
        }
    })
    
    socket.on('stoppedTyping', function(message) {
        $scope.typingName = null;
    })

    $scope.userTyping = function() {
        if($scope.message) {
            socket.emit('typing');
        } else {
            socket.emit('stoppedTyping');
        }
    }
    
    $scope.sendMessage = function() {
    	if($scope.message) {
    	    socket.emit('message',$scope.message);
    	    socket.emit('stoppedTyping');
    	    $('#typeMsg').val('');
    	    $scope.scroll();
    	}
    }
    
    
}]);
