'use strict';

/**
 * @ngdoc overview
 * @name chatRoomApp
 * @description
 * # chatRoomApp
 *
 * Main module of the application.
 */
angular
  .module('chatRoomApp', [
    'ngCookies',
    'ui.router',
    'btford.socket-io',
    'angular-md5'
  ])
  .config(function ($stateProvider,$urlRouterProvider,$locationProvider) {
    $stateProvider
      .state('login', {
        url: '/',
        templateUrl: 'views/loginPage.html',
        controller: 'LoginCtrl',
      })
      .state('groups', {
        url:'/groups',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
      })
      .state('groups.groupPage', {
        url: '/:groupName',
        templateUrl:'views/group.html',
        controller: 'GroupCtrl'
      }) 
    
      $urlRouterProvider.otherwise("/");
  });
