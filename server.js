var http = require('http');
var path = require('path');

var async = require('async');
var socketio = require('socket.io');
var express = require('express');
var mongoose = require('mongoose');

var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

mongoose.connect('mongodb://localhost/groups');

var myText = mongoose.Schema({
  name: String,
  msg: String,
  date: Date,
  email: String
});

var Text = mongoose.model('Text',myText);

var msgSchema = mongoose.Schema({
  data: [myText],
  group: String
});

var Message = mongoose.model('Message',msgSchema); 

router.use(express.static(path.resolve(__dirname, 'app')));
var messages = [];
var sockets = [];
var groups = [];

readGroupsFromDatabase();

io.on('connection', function (socket) {
    var index;
    sockets.push(socket);

    socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      updateUsers(index);
    });
    
    socket.on('identify', function (user) {
      socket.set('name', String(user.name || 'Anonymous'), function (err) {
        if(err) {
          console.error('error imena');
        } else {
          socket.set('email', String(user.email || 'Anonymous'), function (err) {
            if(err) {
              console.error('error email')
            } else {
              console.log(user);
              updateUsers();
            }
          });
        }
      });
      
    });
    
    socket.on('createGroup', function(name) {
        var indexOfGroup = -1;
        
        for(var i=0;i<groups.length;i++) {
             if(groups[i].name == name) {
              indexOfGroup = i;
              break;
            }
        }
          
        if(name!='' && indexOfGroup==-1) {
          socket.set('group',name);
          groups.push({
            name: name,
            users: []
          });
          
          index = groups.length-1;
          addGroupToDatabase(name);
          
          messages.push({
            data:[]

          });
          
          groups[index].users.push(socket);
        }
    });
    
    socket.on('joinGroup', function(name) {
        var tempIndex = -1;
        for(i=0;i<groups.length;i++) {
          if(groups[i].name == name) {
            tempIndex = i;
            break;
          }
        }
        socket.set('group',name);
        if(tempIndex != -1) {
          groups[tempIndex].users.push(socket);
          index = tempIndex;

          var dup = false;
          
          for(i = 0;i<groups[tempIndex].users.length;i++) {
            if(groups[tempIndex].users[i]==socket) {
              if(dup) {
                groups[tempIndex].users.splice(i,1);
              }
              dup = true;
            }
          }
        }
    });
    
    socket.on('getGroups', function() {
      groups.forEach(function(data) {
        socket.emit('groups', data.name);
      })
    });
    
    socket.on('getMessages', function() {
      if(messages[index].data) {
       messages[index].data.forEach(function (data) {
         if(data) {
          socket.emit('message', data);
         }
      });
      }
    });
    
    socket.on('getGroupMessages', function() {
      for(var i=0;i<messages.length;i++) {
        if(messages[i].data) {
         var data = {
            group: groups[i].name,
            msgs: messages[i].data
         };
            socket.emit('groupMessages', data);
        }
      }
    });
    
    socket.on('typing',function(){
      socket.get('name', function (err, name) {
        broadcast('typing',name,index);
      });
    });
    
    socket.on('stoppedTyping', function() {
        socket.get('name',function(err, name) {
            broadcast('stoppedTyping',name,index);
        })
    });
    
    socket.on('message', function (msg) {
      if(index>=0) {
      var text = String(msg || '');

      if (!text)
        return;
        
      var username;
      var email;
      
      socket.get('name', function (err, name) {
        username = name;
      });
      
      socket.get('email', function(err, name) {
          email = name;
      });
      
      var data = {
        group: groups[index].name,
        name: username,
        msg: text,
        date: new Date(),
        email:email
      }
      
      io.sockets.emit('message',data);
      addToDatabase(data,index);
      messages[index].data.push(data);
      }
    });
  });
  
function addToDatabase(data,index) {
  var query = {
    group:groups[index].name
  };
  
  var text = new Text({
    name: data.name,
    msg: data.msg,
    date: data.date,
    email: data.email
  })
  
  Message.update({'group':groups[index].name},{'$push':{
    data: text
  }},{'upsert':true},function(err) {
    if(err) {
      console.error(err);
    } else {
      console.log('dodao')
    }
  })
}

function addGroupToDatabase(name) {
  var query = Message.findOne({group:name});
  
  if(query) {
  var group = new Message({
    group: name,
    data: []
  })
  
  group.save();

  }
}

function readGroupsFromDatabase() {
  var query = Message.find();
  
  query.exec(function(err,message) {
    if(err) {
      console.error(err);
    }
    
    message.forEach(function(grp) {
      groups.push({
        name: grp.group,
        users: []
      })  
      
      messages.push({
        data: grp.data
      })
      
    })
  })
}

function updateUsers() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      console.error(err);
    }
  );
}

function broadcast(event, data,index) {
  if(index>=0) {
    groups[index].users.forEach(function (socket) {
      socket.emit(event, data);
    });
  } 
}

server.listen(router.get('port'), function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.port);
});
